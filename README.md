# ATMDispenser

This is ATM Dispenser project 
This is ATM Dispenser REST service over POST. 
Link to GitLab - https://gitlab.com/NavreetBrar/atmdispenser
Service Endpoint - http://localhost:8080/AtmDispenser/dispenseCash
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

ATMDispenser GIT project contains :
	1. README file. Contains information about whole project.
	2. Intelli ATM source code
	3. Postman collection test suite
	4. Sonarlint folder which contains screenshots for sonarlint reports generated using sonarlint plugin on eclipse. 
	5. Junit results folder which contains unit Test reports
	6. sampleRequestResponse folder which contains one sample of request and response

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Technologies/Tools used:

Framework used : SpringBoot 
Build tool used : Maven
IDE used : Eclipse
Programming language: Java
Repository used : GitLab
Unit Testing Framework used : Junit/Mockito and Postman collection
Code Analysis Tool used : SonarLint

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
To run the service - use maven goal as following:
spring-boot:run
Endpoint - http://localhost:8080/AtmDispenser/dispenseCash

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Additional Test Cases:
 1. invalidCardNumber_NotNumeric :- This request test if the given card number constains only digits or not. If it does not contain only digits, the request validation fails
 2. invalidCardNumber_Length :- This request test if the given card number is atleast 16 digits long. If not, the request validation fails.
 3. withdrawlAmount_0 :- This request test the service for dispensing $0. If the withdrawl amount entered is $0, then request validation fails.
 4  withdrawlAmount_500 :- This request test the service for dispensing $500.
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
 Intelligent ATM dispenser business Logic description:

This is an Intelligent ATM dispenser application which will operate with below specifications/assumptions :
	1. It will take card number as input and check for below validations :
		Entered card number must be of 16 digits.
		Entered card number must be numeric.
	2. It will dispense notes of $500 , $50 and $20 based on the certain conditions.
	3. It will check if the amount to be withdrawn satisfies the combination of denominations and will withdraw only if the exact match is found using minimum number of notes.
		
For example : 
1. Withdrawal amount $110 does not satisfy any combination of 500/50/20 and hence will not be withdrawn.
2. $90 is a valid combination($50+$20+$20) and hence can be withdrawn.
	a. It will check if the entered amount is greater than 0 and fails otherwise with an appropriate message.
	b. It is assumed that there are limited number of notes per denomination: for $500, $50 and $20 there are 50 notes each(These are being set in the main application class using constant file). And once the ATM is loaded (the application is started), the counter of particular notes is reduced every time a withdrawal request comes and is successfully served. For example :Request 1 : Withdraw $90 : It will be success. $50+$20+$20 will be withdrawn. That means 1 note of 50 and 2 notes of 20 will be reduced from the total count i.e. now the $50 note count will be 49 and $20 count will be 48.
	c. Subsequently, if after N requests the count of $50 notes becomes 0. So then the intelligent ATM dispenser will look for $20 notes when a new request comes. 
3. It will display "Out Of cash" when the count of all the denominations becomes 0.
4. If withdrawal amount is greater than total amount(sum of all denominations), then "Insufficient funds" message is thrown back.

** In order to update the count, please update the below mentioned constants in constants file, rebuild and launch the service again.
 DENOMINATION_FIVE_HUNDRED_COUNT 
 DENOMINATION_FIFTY_COUNT 
 DENOMINATION_TWENTY_COUNT 
  
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------
 