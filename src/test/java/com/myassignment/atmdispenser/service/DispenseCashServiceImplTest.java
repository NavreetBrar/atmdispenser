package com.myassignment.atmdispenser.service;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import com.myassignment.atmdispenser.model.ApplicationContext;
import com.myassignment.atmdispenser.model.Currency;
import com.myassignment.atmdispenser.model.DispenseCashResponse;

@RunWith(MockitoJUnitRunner.class)
public class DispenseCashServiceImplTest {

	DispenseCashResponse response = new DispenseCashResponse ();

	@InjectMocks
	private DispenseCashServiceImpl dispenseCashServiceImplObj;
	@Mock
	ApplicationContext appCntxtMock;	
	@Mock
	Currency fiveHundredCurrncy;	
	@Mock
	Currency fiftyCurrency;	
	@Mock
	Currency twentyCurrency;
	
	@Before
	public void setup(){
		Mockito.when(appCntxtMock.setTotalAtmCash(28500)).thenReturn(28500);
		Mockito.when(appCntxtMock.getTotalAtmCash()).thenReturn(28500);
		Mockito.when(fiveHundredCurrncy.totalAmount()).thenReturn(25000);
		Mockito.when(fiftyCurrency.totalAmount()).thenReturn(2500);
		Mockito.when(twentyCurrency.totalAmount()).thenReturn(1000);
		Mockito.when(fiveHundredCurrncy.getDenoCount()).thenReturn(50);
		Mockito.when(fiftyCurrency.getDenoCount()).thenReturn(50);
		Mockito.when(twentyCurrency.getDenoCount()).thenReturn(50);
	}
	
	@Test
	public void withdrawlAmount_320() {
		System.out.println("withdrawlAmount_320");		
		dispenseCashServiceImplObj.dispenseCash(320, response);
		assertEquals("SUCCESS",response.getResponseStatus().toString());	
	}
	
	
	@Test
	public void withdrawlAmount_90() {
		System.out.println("withdrawlAmount_90");
		dispenseCashServiceImplObj.dispenseCash(90, response);
		assertEquals("SUCCESS",response.getResponseStatus().toString());	
	}

	@Test
	public void withdrawlAmount_130() {
		System.out.println("withdrawlAmount_130");
		dispenseCashServiceImplObj.dispenseCash(130, response);
		assertEquals("FAILURE",response.getResponseStatus().toString());	
	}

	@Test
	public void withdrawlAmount_150() {
		System.out.println("withdrawlAmount_150");
		dispenseCashServiceImplObj.dispenseCash(150, response);
		assertEquals("SUCCESS",response.getResponseStatus().toString());	
	}

	@Test
	public void withdrawlAmount_80() {
		System.out.println("withdrawlAmount_80");
		dispenseCashServiceImplObj.dispenseCash(80, response);
		assertEquals("FAILURE",response.getResponseStatus().toString());	
	}

	@Test
	public void withdrawlAmount_10() {
		System.out.println("withdrawlAmount_10");
		dispenseCashServiceImplObj.dispenseCash(10, response);
		assertEquals("FAILURE",response.getResponseStatus().toString());	
	}

	@Test
	public void withdrawlAmount_95() {
		System.out.println("withdrawlAmount_95");
		dispenseCashServiceImplObj.dispenseCash(95, response);
		assertEquals("FAILURE",response.getResponseStatus().toString());		
	}

	@Test
	public void withdrawlAmount_550() {
		System.out.println("withdrawlAmount_550");
		dispenseCashServiceImplObj.dispenseCash(550, response);
		assertEquals("FAILURE",response.getResponseStatus().toString());	
	}
	
	@Test
	public void withdrawlAmount_500() {
		System.out.println("withdrawlAmount_500");
		dispenseCashServiceImplObj.dispenseCash(500, response);
		assertEquals("SUCCESS",response.getResponseStatus().toString());	
	}
	
	@Test
	public void withdrawlAmount_0() {
		System.out.println("withdrawlAmount_0");
		dispenseCashServiceImplObj.dispenseCash(0, response);
		assertEquals("FAILURE",response.getResponseStatus().toString());	
	}
}
