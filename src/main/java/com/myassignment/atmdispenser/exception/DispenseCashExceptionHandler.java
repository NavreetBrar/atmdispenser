package com.myassignment.atmdispenser.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import com.myassignment.atmdispenser.model.DispenseCashResponse;

/**
 * This is class for global exception handling using controllerAdvice annotation
 * 
 * @author Navreet_Brar
 * @version 1.0
 *
 */
@ControllerAdvice(basePackages = {"com.myassignment.atmdispenser"} )
@RestController
public class DispenseCashExceptionHandler {
	private static final Logger LOG = LoggerFactory.getLogger(DispenseCashExceptionHandler.class);	
	DispenseCashResponse response = new DispenseCashResponse();
	
	@ExceptionHandler(ArithmeticException.class)
	public final ResponseEntity<DispenseCashResponse> handleArithmeticException(ArithmeticException ex)  {		
		response.setResponseMessage(ex.getMessage());
        response.setResponseStatus(DispenseCashResponse.responseStatusValues.FAILURE);
        LOG.info(ex.toString());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);       
	}
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<DispenseCashResponse> handleAllExceptions(Exception ex)  {		
		response.setResponseMessage(ex.getMessage());
        response.setResponseStatus(DispenseCashResponse.responseStatusValues.FAILURE);
        LOG.info(ex.toString());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);       
	}
}
