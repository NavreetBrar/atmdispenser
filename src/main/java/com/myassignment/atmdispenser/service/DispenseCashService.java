package com.myassignment.atmdispenser.service;

import com.myassignment.atmdispenser.model.DispenseCashResponse;


/**
 * This is a interface for service class. 
 * 
 * @author Navreet_Brar
 * @version 1.0
 *
 */
public interface DispenseCashService {
	public void dispenseCash(int withdrawlAmount, DispenseCashResponse response);
}
