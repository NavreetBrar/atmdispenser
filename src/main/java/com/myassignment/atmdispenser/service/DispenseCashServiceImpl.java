package com.myassignment.atmdispenser.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.myassignment.atmdispenser.model.ApplicationContext;
import com.myassignment.atmdispenser.model.Currency;
import com.myassignment.atmdispenser.model.DispenseCashResponse;
import com.myassignment.atmdispenser.util.AtmDispenserConstants;

/**
 * This is a service class which contains
 * the business logic. 
 * 
 * @author Navreet_Brar
 * @version 1.0
 *
 */
@Service
public class DispenseCashServiceImpl implements DispenseCashService{
	private static final Logger LOG = LoggerFactory.getLogger(DispenseCashServiceImpl.class);	
	private static int notesOfFiveHundred;
	private static int notesOfFifty;
	private static int notesOfTwenty;

	@Autowired
	ApplicationContext appCntxt ;
	
	@Autowired
	@Qualifier("FiveHundredCurrency")
	Currency fiveHundredCurrncy;

	@Autowired
	@Qualifier("FiftyCurrency")
	Currency fiftyCurrency;

	@Autowired
	@Qualifier("TwentyCurrency")
	Currency twentyCurrency;

	/* (non-Javadoc)
	 * @see com.myassignment.atmdispenser.service.DispenseCashService#dispenseCash(int, com.myassignment.atmdispenser.model.DispenseCashResponse)
	 */
	public void dispenseCash(int withdrawlAmount, DispenseCashResponse response){
		updateFiveHundredNotes(0);
		updateFiftyNotes(0);
		updateTwentyNotes(0);
		
		//setting the total cash available(Fixed amount) in ATM
		appCntxt.setTotalAtmCash(fiveHundredCurrncy.totalAmount()+
				fiftyCurrency.totalAmount()+twentyCurrency.totalAmount());

		if(appCntxt.getTotalAtmCash()==0){
			//return failure response
			response.setResponseStatus(DispenseCashResponse.responseStatusValues.FAILURE);
			response.setResponseMessage(AtmDispenserConstants.NO_CASH);
			return;
		}else if( withdrawlAmount > appCntxt.getTotalAtmCash()){
			//return failure response
			response.setResponseStatus(DispenseCashResponse.responseStatusValues.FAILURE);
			response.setResponseMessage(AtmDispenserConstants.INSUFFICIENT_FUND);
			return;
		}else if( withdrawlAmount > AtmDispenserConstants.DENOMINATION_FIVE_HUNDRED){
			//return failure response
			response.setResponseStatus(DispenseCashResponse.responseStatusValues.FAILURE);
			response.setResponseMessage(AtmDispenserConstants.MAX_WITHDRAWL_AMOUNT_ERROR);
			return;
		}
		if(withdrawlAmount%500==0 && fiveHundredCurrncy.getDenoCount()>0){
			updateFiveHundredNotes(1);
		}else{		
			dispenseNotesOfFifty(withdrawlAmount);		
		}	
		//calling onCompletion to set the response message
		onCompletion(notesOfFiveHundred,notesOfFifty,notesOfTwenty,response,withdrawlAmount);
	}

	/**
	 * @param withdrawlAmount
	 * @param response
	 * @return notesOfFifty
	 * 
	 * This method checks if notes of denomination fifty
	 * can be dispensed. Also checks if notes of denomination twenty
	 * is required to be dispensed
	 * 
	 */
	private void dispenseNotesOfFifty(int withdrawlAmount){	
		int remainderAmount=0;
		updateFiftyNotes(withdrawlAmount/50);
		if(fiftyCurrency.getDenoCount() >= notesOfFifty){
			remainderAmount = withdrawlAmount-(notesOfFifty*AtmDispenserConstants.DENOMINATION_FIFTY);
		}
		else {
			remainderAmount = withdrawlAmount-fiftyCurrency.totalAmount();
			updateFiftyNotes(fiftyCurrency.getDenoCount());
		}
		if(remainderAmount>0)
			dispenseNotesOfTwenty(remainderAmount);
	}

	/**
	 * 
	 * @param withdrawlAmount
	 * @param response
	 * @return notesOfTwenty
	 * 
	 * This method checks if notes of denomination twenty
	 * can be dispensed 
	 * 
	 */
	private void dispenseNotesOfTwenty(int withdrawlAmount) {
		updateTwentyNotes(withdrawlAmount/20);
		if(!(twentyCurrency.getDenoCount() >= notesOfTwenty && withdrawlAmount%20==0)){
			updateFiftyNotes(0);
			updateTwentyNotes(0);
		}
	}
	
	/**
	 * @param notesOfFiveHundred
	 * @param notesOfFifty
	 * @param notesOfTwenty
	 * @param response
	 * 
	 * This method updates denomination count and total atm cash
	 * along with setting successful response message if withdrawal
	 * succeeded
	 * 
	 */
	private void onCompletion(int notesOfFiveHundred, int notesOfFifty, int notesOfTwenty, DispenseCashResponse response,int withdrawlAmount){
		 List<String> respMsgDetailsList = new ArrayList<>();
		 //calculating total dispensable amount
		int dispensableAmt= notesOfFiveHundred*AtmDispenserConstants.DENOMINATION_FIVE_HUNDRED
				+ notesOfFifty*AtmDispenserConstants.DENOMINATION_FIFTY
				+ notesOfTwenty*AtmDispenserConstants.DENOMINATION_TWENTY;
		if(withdrawlAmount==dispensableAmt){
			fiveHundredCurrncy.reduceCount(notesOfFiveHundred);
			fiftyCurrency.reduceCount(notesOfFifty);
			twentyCurrency.reduceCount(notesOfTwenty);
			
			//creating list of denominations to be dispensed
			respMsgDetailsList.add(String.format("$500 denomination notes withdrawn: %s", notesOfFiveHundred));
			respMsgDetailsList.add(String.format("$50 denomination notes withdrawn: %s", notesOfFifty));
			respMsgDetailsList.add(String.format("$20 denomination notes withdrawn: %s", notesOfTwenty));
			//return success message
			response.setResponseStatus(DispenseCashResponse.responseStatusValues.SUCCESS);
			response.setResponseMessage(AtmDispenserConstants.CASH_WITHDRAWL_SUCCESS);
			response.setResponseMessageDetails(respMsgDetailsList);			
			LOG.info("------------------------------------------------------------------------------");
			LOG.info(respMsgDetailsList.toString());
			LOG.info("------------------------------------------------------------------------------");
		}else{
			//return failure response
			response.setResponseStatus(DispenseCashResponse.responseStatusValues.FAILURE);
			response.setResponseMessage(AtmDispenserConstants.CASH_DISPENSE_FAILURE);
			LOG.info("------------------------------------------------------------------------------");
			LOG.info("Un-successful Withdrawl");
			LOG.info("------------------------------------------------------------------------------");
		}
	}
	
	private static void updateFiveHundredNotes(int notes){
		notesOfFiveHundred=notes;
	}
	private static void updateFiftyNotes(int notes){
		notesOfFifty=notes;
	}
	private static void updateTwentyNotes(int notes){
		notesOfTwenty=notes;
	}
}
