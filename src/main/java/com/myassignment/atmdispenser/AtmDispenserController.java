package com.myassignment.atmdispenser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myassignment.atmdispenser.model.DispenseCashRequest;
import com.myassignment.atmdispenser.model.DispenseCashResponse;
import com.myassignment.atmdispenser.service.DispenseCashService;

/**
 * This is a Controller class which is responsible
 * for launching the given service end points using POST and 
 * delegating the control to service class depending upon
 * operation requested
 *   
 * 
 * @author Navreet_Brar
 * @version 1.0
 *
 */
@RestController
@RequestMapping("/AtmDispenser")
public class AtmDispenserController {
	
	@Autowired
	DispenseCashService dispenseCashSvc;

	/**
	 * @param dispenseCashReq
	 * @return ResponseEntity
	 * 
	 * This method is used to perform the operation of dispensing cash
	 * if request passes validation, otherwise returns
	 * failure response message.
	 */
	@PostMapping(value = "/dispenseCash", produces = "application/json")
	public ResponseEntity<DispenseCashResponse> dispenseCash(@RequestBody DispenseCashRequest dispenseCashReq) {
		DispenseCashResponse response = new DispenseCashResponse();
		if(dispenseCashReq.isRequestValid()){
			//call cash dispensing service
			 dispenseCashSvc.dispenseCash(dispenseCashReq.getAmount(),response);
			 return new ResponseEntity<>(response, HttpStatus.OK);
		}else{
			//send validation failure response
			response.setResponseMessage("Input Validation Errors: "+dispenseCashReq.getValidationErrors());
			response.setResponseStatus(DispenseCashResponse.responseStatusValues.FAILURE);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}		
	}
}
