package com.myassignment.atmdispenser.util;

/**
 * This is a utility class used to hold constants. 
 * 
 * @author Navreet_Brar
 * @version 1.0
 *
 */
public final class AtmDispenserConstants {
	
	private AtmDispenserConstants(){
		//private empty constructor to hide the implicit public constructor
	}
	
	public static final String CASH_WITHDRAWL_SUCCESS = "Amount withdrawn successfully. Please collect cash.";
	public static final String INSUFFICIENT_FUND = "Insufficient Funds.";
	public static final String NO_CASH = "ATM Out Of Cash.";
	public static final int DENOMINATION_FIVE_HUNDRED = 500;
	public static final int DENOMINATION_FIFTY = 50;
	public static final int DENOMINATION_TWENTY = 20;
	public static final int DENOMINATION_FIVE_HUNDRED_COUNT = 50;
	public static final int DENOMINATION_FIFTY_COUNT = 50;
	public static final int DENOMINATION_TWENTY_COUNT = 50;
	public static final String INVALID_WITHDRAWL_AMOUNT_ERROR = "Please enter valid amount";
	public static final String MAX_WITHDRAWL_AMOUNT_ERROR = "Maximum $500 can be withdrawn per transaction";
	public static final String INVALID_CARD_NUM_LENGTH = "Please enter valid 16 digit cardNumber";
	public static final String NON_NUMERIC_CARD_NUM = "Please enter valid cardNumber in digits";
	public static final String CASH_DISPENSE_FAILURE = "ATM cannot dispense this Amount, Please choose another Amount.";


}
