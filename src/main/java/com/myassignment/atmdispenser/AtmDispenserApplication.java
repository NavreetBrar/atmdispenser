package com.myassignment.atmdispenser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import com.myassignment.atmdispenser.model.Currency;
import com.myassignment.atmdispenser.util.AtmDispenserConstants;

/**
 * This is main Atm Dispenser Application class. 
 * 
 * @author Navreet_Brar
 * @version 1.0
 *
 */
@SpringBootApplication
public class AtmDispenserApplication {

	private static final Logger LOG = LoggerFactory.getLogger(AtmDispenserApplication.class);	
	
	public static void main(String[] args) {
		SpringApplication.run(AtmDispenserApplication.class, args);
		
		 LOG.info("  >>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
         LOG.info("  >>     ATM Dispenser Service is STARTED       <<");
         LOG.info("  >>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		
	}
	
	@Bean("FiveHundredCurrency")
	public Currency fiveHundredCurrncy(){
		return new Currency(AtmDispenserConstants.DENOMINATION_FIVE_HUNDRED, AtmDispenserConstants.DENOMINATION_FIVE_HUNDRED_COUNT);
	}
	
	@Bean("FiftyCurrency")
	public Currency fiftyCurrncy(){
		return new Currency(AtmDispenserConstants.DENOMINATION_FIFTY, AtmDispenserConstants.DENOMINATION_FIFTY_COUNT);
	}
	
	@Bean("TwentyCurrency")
	public Currency twentyCurrncy(){
		return new Currency(AtmDispenserConstants.DENOMINATION_TWENTY, AtmDispenserConstants.DENOMINATION_TWENTY_COUNT);
	}
	
}
