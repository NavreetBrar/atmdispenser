package com.myassignment.atmdispenser.model;

import java.util.List;

import org.springframework.stereotype.Component;

/**
 * This is a POJO class which holds all the response data. 
 * 
 * @author Navreet_Brar
 * @version 1.0
 *
 */
@Component
public class DispenseCashResponse {

	public enum responseStatusValues{
		SUCCESS, FAILURE
	}

	private responseStatusValues responseStatus;
	private String responseMessage;
	private List<String> responseMessageDetails;

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public responseStatusValues getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(responseStatusValues responseStatus) {
		this.responseStatus = responseStatus;
	}

	public List<String> getResponseMessageDetails() {
		return responseMessageDetails;
	}

	public void setResponseMessageDetails(List<String> responseMessageDetails) {
		this.responseMessageDetails = responseMessageDetails;
	}

	

}
