package com.myassignment.atmdispenser.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;

import com.myassignment.atmdispenser.util.AtmDispenserConstants;

/**
 * This is a POJO class which maps all the request data. 
 * Additionally, it also does validation of the required request attributes.
 * 
 * @author Navreet_Brar
 * @version 1.0
 *
 */

@Component
public class DispenseCashRequest {
	private String cardNumber;
	private int amount;
	private List<String> errorList = new ArrayList<>();

	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}

	/**
	 * @return boolean 
	 * This returns true is request is valid, otherwise false
	 */
	public boolean isRequestValid(){
		boolean isValid = true;
		if(this.cardNumber!=null && !this.cardNumber.isEmpty()){
			if (this.cardNumber.length()!=16){
				errorList.add(AtmDispenserConstants.INVALID_CARD_NUM_LENGTH);
				isValid=false;
			}
			if (!(this.cardNumber.chars().allMatch(Character::isDigit))){
				errorList.add(AtmDispenserConstants.NON_NUMERIC_CARD_NUM);
				isValid=false;
			}
		}else{
			errorList.add(String.format("['%s' cannot be empty]", this.cardNumber));
			isValid=false;
		}
		//$0 is invalid withdrawl amount
		if(this.amount==0){
			errorList.add(AtmDispenserConstants.INVALID_WITHDRAWL_AMOUNT_ERROR);
			isValid=false;
		//More than $500 can't be withdrawn in one transaction
		}else if(this.amount>500){			 
			errorList.add(AtmDispenserConstants.MAX_WITHDRAWL_AMOUNT_ERROR);
			isValid=false;
		}
		return isValid;
	}

	
	/**
	 * @return List<String> 
	 * This returns the list of error messages if the request is invalid
	 */
	public List<String> getValidationErrors(){
		return Collections.unmodifiableList(this.errorList);
	}

}
