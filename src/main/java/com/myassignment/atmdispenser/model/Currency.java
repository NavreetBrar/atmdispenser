package com.myassignment.atmdispenser.model;

/**
 * This is a POJO class for holding information about
 * currency denomination and count of notes
 * of that respective denomination 
 *   
 * 
 * @author Navreet_Brar
 * @version 1.0
 *
 */
public class Currency {
	private int denomination;
	private int denoCount;

	public Currency(int denomination, int denoCount) {
		super();
		this.denomination = denomination;
		this.denoCount = denoCount;
	}

	public int getDenomination() {
		return denomination;
	}

	public int getDenoCount() {
		return denoCount;
	}

	/**
	 * @param noteCount 
	 * This method accepts "count of notes requested for withdrawal"
	 * as input parameter and decrements it from the total 
	 * count of the respective denomination
	 * 
	 */
	public void reduceCount(int noteCount){
		this.denoCount = this.denoCount-noteCount;
	}
	
	/**
	 * @return int
	 * This method calculates the total amount available of
	 * a particular denomination by multiplying 
	 * the count with respective currency denomination
	 */
	public int totalAmount(){
		return this.denoCount*this.denomination;
	}

}
