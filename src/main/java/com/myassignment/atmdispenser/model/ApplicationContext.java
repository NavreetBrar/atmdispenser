package com.myassignment.atmdispenser.model;

import javax.annotation.ManagedBean;
import org.springframework.web.context.annotation.ApplicationScope;


/**
 * This is a model class which holds application level data. 
 * 
 * @author Navreet_Brar
 * @version 1.0
 *
 */
@ManagedBean
@ApplicationScope
public class ApplicationContext {
	private int totalAtmCash;

	public int getTotalAtmCash() {
		return totalAtmCash;
	}

	public int setTotalAtmCash(int totalAtmCash) {
		this.totalAtmCash = totalAtmCash;
		return this.totalAtmCash;
	}	
	
	
}
